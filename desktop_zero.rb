# Use AppleScript to display a message dialog. Used for errors.
def display_alert(title, msg)
  title = title.to_s.gsub('"', '\"').gsub("'", '`')
  msg = msg.to_s.gsub('"', '\"').gsub("'", '`')
  %x{osascript -e 'tell application "Finder" to display alert "#{title}" message "#{msg}"'}
end

# Wrap everything in a begin/rescue block so an error message can shown in the UI.
begin

require 'fileutils'
require 'date'
require 'shellwords'

ALIAS_MAGIC_NUMBER = "\x62\x6F\x6F\x6B\x00\x00\x00\x00\x6D\x61\x72\x6B\x00\x00\x00\x00"
ARCHIVE_NAME_FORMAT = '%Y-%m %B' # E.g. '2013-02 February'
ARCHIVE_NAME_PATTERN = /^(?<year>\d{4})-(?<month>\d{2})/ # Needs to match above format so the date can be parsed out later.
MONTHS_TO_KEEP_UNCOMPRESSED = 4 # Includes the current, partial, month.
DESKTOP_PATH = "#{ENV['HOME']}/Desktop/"
ARCHIVE_BASE_PATH = "#{ENV['HOME']}/Desktop Zero/"
NOW = Time.now

files_to_archive = Dir.new(DESKTOP_PATH).select do |filename|
  result = true

  # Exclude dotfiles.
  if filename.start_with? '.'
    result = false
  end

  file_path = "#{DESKTOP_PATH}#{filename}"

  # Exclude aliases.
  if File.file?(file_path) and File.open(file_path, 'r').read(16) == ALIAS_MAGIC_NUMBER
    result = false
  end

  result
end

archive_name = NOW.strftime(ARCHIVE_NAME_FORMAT)
archive_path = "#{ARCHIVE_BASE_PATH}#{archive_name}/"

# Create archive folder if it doesn't exist.
FileUtils.mkdir_p archive_path

# Move files into archive folder.
files_to_archive.each do |filename|
  file_path = "#{DESKTOP_PATH}#{filename}"
  destination_path = "#{archive_path}#{filename}"

  # Check for name conflict.
  if File.exists? destination_path
    # File already in archive, try e.g. 'File 1.jpg'
    filename_without_ext = File.basename(filename, '.*')
    filename_ext = File.extname(filename)
    number = 1
    dirname = File.dirname(destination_path)
    while File.exists?(destination_path = File.join(dirname, "#{filename_without_ext} #{number}#{filename_ext}"))
      # If 'File 1.jpg' exists, find the next number that doesn't, e.g. 'File 2.jpg', 'File 3.jpg', etc.
      number += 1
    end
  end

  FileUtils.move file_path, destination_path
end

compress_until = Date.new(NOW.year, NOW.month) << MONTHS_TO_KEEP_UNCOMPRESSED

archives_to_compress = Dir.new(ARCHIVE_BASE_PATH).select do |filename|
  # Only consider directories with correct format of name.
  result = false
  file_path = "#{ARCHIVE_BASE_PATH}#{filename}"
  if not file_path.start_with? '.'
    if File.directory? file_path
      if ARCHIVE_NAME_PATTERN =~ filename
        archive_date = Date.new($~[:year].to_i, $~[:month].to_i)

        if archive_date <= compress_until
          result = true
        end
      end
    end
  end
  result
end

archives_to_compress.each do |filename|
  %x{cd #{Shellwords.escape(ARCHIVE_BASE_PATH)}; zip -r #{Shellwords.escape filename} #{Shellwords.escape filename}}
  if $? == 0
    FileUtils.remove_dir "#{ARCHIVE_BASE_PATH}#{filename}"
  end
end

rescue Exception => e
  display_alert('Desktop Zero Error', "#{e.to_s}\n#{e.backtrace.to_s}")
end
