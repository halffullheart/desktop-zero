## About

Desktop Zero is a script that moves all\* files on the desktop into an archive folder named based on the current month, like `~/Desktop Zero/2015-12 December`. The idea is you can continue to litter your desktop with files as usual but now every once in a while you press a key combination and they all disappear! Sweet relief. No more clutter. But your stuff is still there in the archive if you really need it.

But there’s more! Each time Desktop Zero runs, it creates new archive directories as needed and it zips up the older ones to save disk space. So after using it for a few months you’ll have something like this in your `~/Desktop Zero`:

    2015-12 December/
    2015-11 November/
    2015-10 October/
    2015-09 September/
    2015-08 August.zip
    2015-07 July.zip

After things get really old and you’ve never gone and unziped one of these, then maybe you’ll feel like throwing it away. If not, then at least you are saving some space with the zip files.

The folder names are configurable by the way. The naming convention is to use the year and month digits and then the name of the month. That way everything sorts nicely and you don’t have to translate numbers into month names. I’m bad at that. Maybe you’re not. Anyway, you can change it. Look for `ARCHIVE_NAME` in the code.

Finally, Desktop Zero is also smart enough to rename files when archiving them if there are conflicts. So if you have Untitled.txt on your desktop and there is already an Untitled.txt in the archive for this month, then the new one being moved from the desktop will be renamed to Untitled 2.txt.

\*Aliases are ignored. So if you have an alias on your desktop that you use as a shortcut it will stay there.

## Install

1. Create a new ”Quick Action” in Automator
2. Change “Workflow receives” to “no input” in “Finder”
3. Drag in a “Run Shell Script” action (listed in “Utilities”)
4. Change the “Shell” to `usr/bin/ruby`
5. Copy and paste the entire [source code](desktop_zero.rb) into the script text field
6. Save the action as “Desktop Zero”
7. Open System Preferences and go to Keyboard > Shortcuts > Services
8. Find “Desktop Zero” in the list and add a shortcut e.g. Command-Opt-Control-D
